package com.epam.training.trayana_vladimirova.task2.pages;

import com.epam.training.trayana_vladimirova.task3.pages.BasePageObject;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.time.Duration;

public class NewPasteForm extends BasePageObject {

    private By advertBanner = By.id("vi-smartbanner");
    private By newPasteContentField = By.cssSelector("textarea#postform-text");
    private By syntaxHighlightingField = By.cssSelector("span#select2-postform-format-container");
    private By pasteExpirationField = By.id("select2-postform-expiration-container");
    private By pasteTitleField = By.cssSelector("input#postform-name");
    private By createNewPasteButton = By.xpath("//form[@action='/']//button[@type='submit']");

    public NewPasteForm(WebDriver driver, Logger log) {
        super(driver, log);
    }

    public void waitForBannerAndClose() {
        waitForVisibilityOf(advertBanner, Duration.ofSeconds(10));
        ((JavascriptExecutor) driver).executeScript("document.getElementById('vi-smartbanner').remove()");
        log.info("Advert banner is closed.");
    }

    public NewPasteForm fillNewPasteContent (String newPasteContent) {
        click(newPasteContentField);
        type(newPasteContent, newPasteContentField);
        log.info("New paste content has been filled.");
        return this;
    }

    public NewPasteForm selectSyntaxHighlighting(String syntaxHighlightingOption) {
        click(syntaxHighlightingField);
        click(By.xpath(syntaxHighlightingOption));
        log.info("Syntax highlighting has been selected.");
        return this;
    }

    public NewPasteForm selectPasteExpiration(String pasteExpirationOption) {
        click(pasteExpirationField);
        click(By.xpath(pasteExpirationOption));
        log.info("Paste expiration has been set");
        return this;
    }

    public void fillPasteTitle (String pasteTitle)  {
        click(pasteTitleField);
        type(pasteTitle, pasteTitleField);
        log.info("Paste title field has been filled");
    }

    public NewPasteConfirmationPage clickCreateNewPaste() {
        click(createNewPasteButton);
        log.info("Create New Paste button has been clicked");
        return new NewPasteConfirmationPage(driver, log);
    }
}
