package com.epam.training.trayana_vladimirova.task2.pages;

import com.epam.training.trayana_vladimirova.task3.pages.BasePageObject;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePageLoggedInUsers extends BasePageObject {

    private By plusPasteButton = By.xpath("//a[2]/span[.='paste']");

    public HomePageLoggedInUsers(WebDriver driver, Logger log) {
        super(driver, log);
    }

    public NewPasteForm clickPlusPasteButton() {
        click(plusPasteButton);
        log.info("+paste button is clicked");
        return new NewPasteForm(driver, log);
    }
}
