package com.epam.training.trayana_vladimirova.task2.pages;

import com.epam.training.trayana_vladimirova.task3.pages.BasePageObject;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginFormPage extends BasePageObject {

    private By userNameField = By.cssSelector("input#loginform-username");
    private By passwordField = By.cssSelector("input#loginform-password");
    private By loginButtonUnderLoginForm = By.xpath("//form[@action='/login']//button[@type='submit']");

    public LoginFormPage(WebDriver driver, Logger log) {
        super(driver, log);
    }

    public HomePageLoggedInUsers performLogin (String username, String password) {
        log.info("Starting login process");
        type(username, userNameField);
        type(password, passwordField);
        click(loginButtonUnderLoginForm);
        log.info("Successful login");
        return new HomePageLoggedInUsers(driver, log);
    }
}
