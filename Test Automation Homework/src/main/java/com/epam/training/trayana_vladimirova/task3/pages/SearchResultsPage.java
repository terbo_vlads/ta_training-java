package com.epam.training.trayana_vladimirova.task3.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchResultsPage extends BasePageObject {

    private By legacyCalculatorLink = By.xpath("//a[@href='https://cloud.google.com/products/calculator-legacy']");

    public SearchResultsPage(WebDriver driver, Logger log) {
        super(driver, log);
    }

    public GoogleCloudCalcPage clickLegacyCalculatorLink() {
        log.info("Clicking the Legacy Calculator Link");
        click(legacyCalculatorLink);

        return new GoogleCloudCalcPage(driver, log);
    }
}
