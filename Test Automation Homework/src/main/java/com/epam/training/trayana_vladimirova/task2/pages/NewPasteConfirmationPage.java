package com.epam.training.trayana_vladimirova.task2.pages;

import com.epam.training.trayana_vladimirova.task3.pages.BasePageObject;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.time.Duration;

public class NewPasteConfirmationPage extends BasePageObject {

    private By newPasteTitle = By.xpath("//h1");
    private By bashButton = By.xpath("/html/body/div[@class='wrap']/div[@class='container']//a[@href='/archive/bash']");
    private By pasteContent = By.xpath("//div[@class='highlighted-code']/div[2]/ol[@class='bash']");

    public NewPasteConfirmationPage(WebDriver driver, Logger log) {
        super(driver, log);
    }

    public String getNewPasteTitle() {
        waitForVisibilityOf(newPasteTitle, Duration.ofSeconds(5));
        return find(newPasteTitle).getText();
    }

    public boolean displayOfBashButton() {
        waitForVisibilityOf(bashButton, Duration.ofSeconds(5));
        WebElement bashButtonElement = find(bashButton);
        return bashButtonElement.isDisplayed();
    }

    public String getNewPasteContent() {
        waitForVisibilityOf(pasteContent, Duration.ofSeconds(5));
        return find(pasteContent).getText();
    }
}
