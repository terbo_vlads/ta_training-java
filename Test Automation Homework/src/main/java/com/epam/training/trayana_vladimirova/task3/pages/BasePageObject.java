package com.epam.training.trayana_vladimirova.task3.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BasePageObject {
    protected WebDriver driver;
    protected Logger log;

    public BasePageObject(WebDriver driver, Logger log) {
        this.driver = driver;
        this.log = log;
    }

    // Open page with the specified URL
    protected void openUrl(String url) {
        driver.get(url);
    }

    // Find element using the specified locator
    protected WebElement find(By locator) {
        return driver.findElement(locator);
    }

    // Click on element with the specified locator when visible
    protected void click(By locator) {
        waitForVisibilityOf(locator, Duration.ofSeconds(10));
        find(locator).click();
    }

    // Type specified text into element with the specified locator
    protected void type(String text, By locator) {
        waitForVisibilityOf(locator, Duration.ofSeconds(10));
        find(locator).sendKeys(text);
    }

    // Type specified text into element with the specified locator and click ENTER
    protected void typeAndClickEnter(String text, By locator) {
        waitForVisibilityOf(locator, Duration.ofSeconds(10));
        find(locator).sendKeys(text + Keys.ENTER);
    }

    // Wait for specific ExpectedCondition for the given duration
    private void waitFor(ExpectedCondition<WebElement> condition, Duration timeOut) {
        timeOut = timeOut != null ? timeOut: Duration.ofSeconds(30);
        WebDriverWait wait = new WebDriverWait(driver, timeOut);
        wait.until(condition);
    }

    // Wait for specified number of seconds for element with a given locator to be visible
    protected void waitForVisibilityOf(By locator, Duration... timeOut) {
        int attempts = 0;
        while(attempts < 2) {
            try {
                waitFor(ExpectedConditions.visibilityOfElementLocated(locator),
                        (timeOut.length > 0 ? timeOut[0]:null));
                break;
            } catch (StaleElementReferenceException e) {

            }
            attempts++;
        }
    }
}
