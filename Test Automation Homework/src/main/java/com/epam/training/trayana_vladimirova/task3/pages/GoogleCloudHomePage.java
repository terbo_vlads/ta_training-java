package com.epam.training.trayana_vladimirova.task3.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import java.time.Duration;

public class GoogleCloudHomePage extends BasePageObject {

    private String homePageUrl = "https://cloud.google.com/";
    private By searchButton = By.xpath("//*[@jsname='Ohx1pb']");
    private By searchBar = By.xpath("//input[@class='mb2a7b' and @jsname='jjXxte']");
    private By cookiesBanner = By.id("glue-cookie-notification-bar-1");
    private By cookiesAcceptButton = By.xpath("//button[@class='glue-cookie-notification-bar__accept']");

    public GoogleCloudHomePage(WebDriver driver, Logger log) {
        super(driver, log);
    }

    // Open Google Cloud Home Page with using its URL
    public void openPage() {
        log.info("Opening page: " + homePageUrl);
        openUrl(homePageUrl);
        log.info("Page opened");
    }

    public void acceptCookies() {
        waitForVisibilityOf(cookiesBanner, Duration.ofSeconds(10));
        click(cookiesAcceptButton);
    }

    public SearchResultsPage performSearch(String searchTerms) {
        log.info("Clicking the search button");
        click(searchButton);
        log.info("Typing the search terms");
        typeAndClickEnter(searchTerms, searchBar);

        return new SearchResultsPage(driver, log);
    }
}
