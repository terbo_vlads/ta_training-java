package com.epam.training.trayana_vladimirova.task3.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GoogleCloudCalcPage extends BasePageObject {

    private By frame1 = By.cssSelector("#cloud-site iframe");
    private final By frame2 = By.id("myFrame");
    private By computeEngineButton = By.xpath("//md-tab-item[@id='tab-item-1']");
    private By numberOfInstancesField = By.xpath(".//input[@ng-model='listingCtrl.computeServer.quantity']");
    private By instancesForField = By.xpath(".//input[@ng-model='listingCtrl.computeServer.label']");
    private By operatingSystemField = By.xpath(".//md-select[@ng-model='listingCtrl.computeServer.os']");
    private By provisioningModelField = By.xpath("//md-select[@ng-model='listingCtrl.computeServer.class']");
    private By machineFamilyField = By.xpath("/html//md-card-content[@id='mainForm']//md-card/md-card-content//form[@name='ComputeEngineForm']/div[5]/div[1]/md-input-container/md-select[@role='listbox']");
    private By seriesField = By.xpath("//md-select[@placeholder='Series']");
    private By machineTypeField = By.xpath("//md-select[@placeholder='Instance type']");
    private By addGPUsCheckBox = By.xpath("//md-checkbox[@ng-model='listingCtrl.computeServer.addGPUs']");
    private By gpuTypeField = By.xpath("//md-select[@placeholder='GPU type']");
    private By numberOfGPUsField = By.xpath("//md-select[@placeholder='Number of GPUs']");
    private By localSSDField = By.xpath("//md-select[@placeholder='Local SSD']");
    private By dataCenterLocationField = By.xpath("//md-select[@placeholder='Datacenter location']");
    private By committedUsageField = By.xpath("//md-select[@placeholder='Committed usage']");
    private By addToEstimateButton = By.xpath("//button[contains(text(), 'Add to Estimate')]");
    private By totalEstimateTextLocator = By.xpath("//b[contains(text(),'Total Estimated Cost')]");


    public GoogleCloudCalcPage(WebDriver driver, Logger log) {
        super(driver, log);
    }

//    public void closeQuestionsMessage() {


    public void clickComputeEngine() {
        log.info("Clicking the Compute Engine Button");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame1));
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame2));

        click(computeEngineButton);

        log.info("Compute Engine button is clicked");

    }

//    public void closeHelpChatButton() {
//        click(helpChatButton);
//    }

    public GoogleCloudCalcPage setNumberOfInstances(int numberOfInstances) {
        click(numberOfInstancesField);
        type(String.valueOf(numberOfInstances), numberOfInstancesField);
        log.info("Number of instances are set");
        return this;
    }

    public GoogleCloudCalcPage setInstancesPurpose(String purpose) {
        WebElement input = find(instancesForField);
        if (purpose == null) {
            input.clear();
        } else {
            type(purpose, instancesForField);
        }
        log.info("Instances For field is left blank");

        return this;
    }

    public GoogleCloudCalcPage setOperatingSystem(String operatingSystemOption) {
        click(operatingSystemField);
        click(By.xpath(operatingSystemOption));
        log.info("Operating system option is selected");

        return this;
    }

    public GoogleCloudCalcPage setProvisioningModel(String provisioningModelOption) {
//        WebElement provisionModelFieldElement = find(provisioningModelField);
//        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", provisionModelFieldElement);
//        log.info("Page is scrolled down.");
        click(provisioningModelField);
        log.info("Provisioning model field is clicked");
        click(By.xpath(provisioningModelOption));
        return this;
    }

    public GoogleCloudCalcPage setMachineFamily(String machineFamilyOption) {
        click(machineFamilyField);
        click(By.xpath(machineFamilyOption));
        log.info("Machine family option is selected");

        return this;
    }

    public GoogleCloudCalcPage setSeries(String seriesOption) {
        click(seriesField);
        click(By.xpath(seriesOption));
        log.info("Series option is selected");

        return this;
    }

    public GoogleCloudCalcPage setMachineType(String machineTypeOption) {
        click(machineTypeField);
        click(By.xpath(machineTypeOption));
        log.info("Machine type is set");

        return this;
    }

    public GoogleCloudCalcPage selectAddGPUs() {
        click(addGPUsCheckBox);
        log.info("Add GPUs button is clicked");
        return this;
    }

    public GoogleCloudCalcPage selectGPUType(String gpuTypeOption) {
        click(gpuTypeField);
        click(By.xpath(gpuTypeOption));
        log.info("GPU type is set");

        return this;
    }

    public GoogleCloudCalcPage selectNumberOfGPUs(String numberOfGPUsOption) {
        click(numberOfGPUsField);
        click(By.xpath(numberOfGPUsOption));
        log.info("GPU number is selected");

        return this;
    }

    public GoogleCloudCalcPage setLocalSSD(String numberOfSSDOption) {
        click(localSSDField);
        click(By.xpath(numberOfSSDOption));
        log.info("Local SSD number is set.");

        return this;
    }

    public GoogleCloudCalcPage setDataCenterLocation(String dataCenterLocationOption) {
        click(dataCenterLocationField);
        click(By.xpath(dataCenterLocationOption));
        log.info("Data Center location is set");

        return this;
    }

    public GoogleCloudCalcPage setCommittedUsage(String committedUsageOption) {
        click(committedUsageField);
        click(By.xpath(committedUsageOption));
        log.info("Committed usage is set");

        return this;
    }

    public GoogleCloudCalcPage clickAddToEstimateButton() {
        click(addToEstimateButton);
        log.info("Add to Estimate button is clicked");

        return this;
    }

    public String getTotalEstimateText() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(totalEstimateTextLocator));
        return driver.findElement(totalEstimateTextLocator).getText();
    }

}
