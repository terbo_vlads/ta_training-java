package com.epam.training.trayana_vladimirova.task3.base;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScreenshotListener implements ITestListener {

    private static final Logger log = LogManager.getLogger(ScreenshotListener.class);

    public ScreenshotListener() {
        System.out.println("ScreenshotListener instance created");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        log.info("Test failed");

        WebDriver driver = BrowserDriverFactory.getDriver();
        if (driver == null) {
            log.error("WebDriver instance is null, cannot take screenshot.");
            return;
        }

        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        BaseTest baseTest = (BaseTest) result.getInstance();
        try {
            File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            String projectRoot = new File("").getAbsolutePath();
            File destFile = new File(projectRoot + "/screenshots/failed_test_" + result.getName() + "_" + timestamp + ".png");
            log.info("Trying to save screenshot to: " + destFile.getAbsolutePath());
            FileUtils.copyFile(srcFile, destFile);
            log.info("Screenshot saved successfully to: " + destFile.getAbsolutePath());
        } catch (Exception e) {
            log.error("Error capturing screenshot: ", e);
            e.printStackTrace();
        }
    }
}
