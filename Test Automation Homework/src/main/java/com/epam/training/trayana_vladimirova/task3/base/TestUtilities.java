package com.epam.training.trayana_vladimirova.task3.base;

import com.epam.training.trayana_vladimirova.task3.pages.BasePageObject;
import org.testng.annotations.BeforeSuite;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TestUtilities extends BaseTest {

    protected static Properties testData;
    //Static explicit wait sleep
    protected void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @BeforeSuite
    public void loadTestData() throws IOException {
        String environment = System.getProperty("test.env", "qa");
        testData = new Properties();
        FileInputStream fis = new FileInputStream("src/test/resources/testData." + environment + ".properties"); // Relative path
        testData.load(fis);
        fis.close(); // Important: Close the FileInputStream
    }
}
