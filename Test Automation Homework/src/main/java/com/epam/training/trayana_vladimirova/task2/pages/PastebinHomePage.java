package com.epam.training.trayana_vladimirova.task2.pages;

import com.epam.training.trayana_vladimirova.task3.pages.BasePageObject;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PastebinHomePage extends BasePageObject {
    private String PastebinHomePageUrl = "https://pastebin.com/";
    private By agreeButton = By.xpath("//button[@class=' css-47sehv']");
    private By loginButton = By.linkText("LOGIN");

    public PastebinHomePage(WebDriver driver, Logger log) {
        super(driver, log);
    }

    public void openPastebinHomePage () {
        openUrl(PastebinHomePageUrl);
    }

    public void clickAgreeButton() {
        click(agreeButton);
        log.info("Agree button is clicked.");
    }

    public LoginFormPage goToLoginPage() {
        click(loginButton);
        return new LoginFormPage(driver, log);

    }
}
