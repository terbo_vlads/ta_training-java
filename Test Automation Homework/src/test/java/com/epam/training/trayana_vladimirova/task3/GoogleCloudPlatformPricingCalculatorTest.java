package com.epam.training.trayana_vladimirova.task3;

import com.epam.training.trayana_vladimirova.task3.base.TestUtilities;
import com.epam.training.trayana_vladimirova.task3.pages.GoogleCloudHomePage;
import com.epam.training.trayana_vladimirova.task3.pages.GoogleCloudCalcPage;
import com.epam.training.trayana_vladimirova.task3.pages.SearchResultsPage;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class GoogleCloudPlatformPricingCalculatorTest extends TestUtilities {

    @Test
    public void testOptionsSelection () {

        // Open https://cloud.google.com/
        GoogleCloudHomePage googleCloudHomePage = new GoogleCloudHomePage(driver, log);
        googleCloudHomePage.openPage();
        googleCloudHomePage.acceptCookies();

        // Perform the search of "Google Cloud Platform Pricing Calculator"
        googleCloudHomePage.performSearch(testData.getProperty("searchTerms"));

        // Open the Google Cloud Pricing Calculator from the Search Results Page
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver, log);
        searchResultsPage.clickLegacyCalculatorLink();

        //Click the Compute Engine Button
        GoogleCloudCalcPage calculatorPage = new GoogleCloudCalcPage(driver, log);
        calculatorPage.clickComputeEngine();

        //Filling in the required information in the Google Cloud Calculator Page and clicking Add to Estimate
        calculatorPage
                .setNumberOfInstances(Integer.parseInt(testData.getProperty("numberOfInstances")))
                .setInstancesPurpose(testData.getProperty("purpose"))
                .setOperatingSystem(testData.getProperty("operatingSystemOption"))
                .setProvisioningModel(testData.getProperty("provisioningModelOption"))
                .setMachineFamily(testData.getProperty("machineFamilyOption"))
                .setSeries(testData.getProperty("seriesOption"))
                .setMachineType(testData.getProperty("machineTypeOption"))
                .selectAddGPUs()
                .selectGPUType(testData.getProperty("gpuTypeOption"))
                .selectNumberOfGPUs(testData.getProperty("numberOfGPUsOption"))
                .setLocalSSD(testData.getProperty("numberOfSSDOption"))
                .setDataCenterLocation(testData.getProperty("dataCenterLocationOption"))
                .setCommittedUsage(testData.getProperty("committedUsageOption"))
                .clickAddToEstimateButton();

        Assert.assertTrue(calculatorPage.getTotalEstimateText().matches("Total Estimated Cost: USD [\\d,\\.]+ per 1 month"),
                "Total estimated cost is not in the expected format");
        log.info("Total estimated cost is in the expected format.");
    }

}
