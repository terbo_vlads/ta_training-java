package com.epam.training.trayana_vladimirova.task2;

import com.epam.training.trayana_vladimirova.task2.pages.*;
import com.epam.training.trayana_vladimirova.task3.base.TestUtilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.time.Duration;

public class PastebinNewBashPaste extends TestUtilities {

    @Parameters({ "username", "password", "newPasteContent",
            "syntaxHighlightingOption", "pasteExpiration", "pasteTitle" })
    @Test
    public void newPasteBash(String username, String password, String newPasteContent,
                             String syntaxHighlightingOption, String pasteExpiration, String pasteTitle) {

        // Go to HomePage, accept privacy policy and go to login page
        PastebinHomePage pastebinHomePage = new PastebinHomePage(driver, log);
        pastebinHomePage.openPastebinHomePage();
        pastebinHomePage.clickAgreeButton();
        LoginFormPage loginFormPage = pastebinHomePage.goToLoginPage();

        // Logging in
        HomePageLoggedInUsers homePageLoggedInUsers = loginFormPage.performLogin(username, password);

        // Click '+paste' button
        NewPasteForm newPasteForm = homePageLoggedInUsers.clickPlusPasteButton();

        // Handling the advert banner
        newPasteForm.waitForBannerAndClose();

        // Create New Paste with the required attributes
        newPasteForm.fillNewPasteContent(newPasteContent)
                .selectSyntaxHighlighting(syntaxHighlightingOption)
                .selectPasteExpiration(pasteExpiration)
                .fillPasteTitle(pasteTitle);
        NewPasteConfirmationPage newPasteConfirmationPage = newPasteForm.clickCreateNewPaste();
        log.info("New paste has been successfully created.");

        // Verification

        //Browser page title matches paste title.
        String expectedTitle = "how to gain dominance among developers";
        String actualTitle = newPasteConfirmationPage.getNewPasteTitle();
        Assert.assertEquals(expectedTitle, actualTitle, "Actual title is not the same as expected");
        log.info("Actual and expected title match");

        // Bash syntax highlighting
        Assert.assertTrue(newPasteConfirmationPage.displayOfBashButton(), "Bash button is not visible");
        log.info("The Bash button displayed shows that Bash syntax highlighting has been selected.");

        // New Paste content
        String expectedContent = "git config --global user.name \"New Sheriff in Town\"\n" +
                "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
                "git push origin master --force";
        String actualContent = newPasteConfirmationPage.getNewPasteContent();
        Assert.assertTrue(actualContent.contains(expectedContent),
                "Actual content does not contain the expected content\n " +
                "Actual content: " + actualContent +
                "\nExpected content: " + expectedContent);
        log.info("The new paste content matches the expected.");

    }
}
