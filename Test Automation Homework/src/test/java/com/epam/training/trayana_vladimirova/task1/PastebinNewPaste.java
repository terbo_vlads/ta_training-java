package com.epam.training.trayana_vladimirova.task1;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class PastebinNewPaste {

    private WebDriver driver;

    @Parameters({"browser"})
    @BeforeMethod(alwaysRun = true)
    private void setUp(@Optional("chrome") String browser) {
        switch(browser) {
            case "chrome":
                driver = new ChromeDriver();
                break;
            case "firefox":
                driver = new FirefoxDriver();
                break;
            case "edge":
                driver = new EdgeDriver();
                break;
            default:
                System.out.println(("Do not know how to start " + browser + ", starting Chrome instead"));
                driver = new ChromeDriver();
                break;
        }
        System.out.println("Browser started");

        driver.manage().window().maximize();
        System.out.println("Page is open");

    }
    @Test
    public void createNewPaste() {
        driver.get("https://pastebin.com/");
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

        // Fill in New Paste content
        checkAndAcceptPrivacyBanner(driver);
        WebElement textArea = driver.findElement(By.xpath("//*[@id=\"postform-text\"]"));
        textArea.sendKeys("Hello from WebDriver");

        // Handling the banner
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("vi-smartbanner")));
        ((JavascriptExecutor) driver).executeScript("document.getElementById('vi-smartbanner').remove()");

        // Set paste expiration
        driver.findElement(By.id("select2-postform-expiration-container")).click();
        driver.findElement(By.xpath("//ul[@id='select2-postform-expiration-results']/li[3]")).click();

        // Filling out "Paste name/Title
        WebElement pasteName = driver.findElement(By.id("postform-name"));
        pasteName.sendKeys("helloweb");

        // Submitting new paste
        WebElement createNewPasteBtn = driver.findElement(By.xpath(
                "//form[@action='/']//button[@type='submit']"));
        createNewPasteBtn.click();

        // Verification

        // Browser page title matches the expected title
        WebDriverWait wait4 = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1")));
        WebElement pageTitle = driver.findElement(By.xpath
                ("//h1"));
        String expectedTitle = "helloweb";
        String actualTitle = pageTitle.getText();

        Assert.assertEquals(expectedTitle, actualTitle, "Actual title is not the same as expected");

        // No syntax highlighting
        WebElement textButton = driver.findElement(By.xpath
                ("/html/body/div[@class='wrap']/div[@class='container']//a[@href='/archive/text']"));
        Assert.assertTrue(textButton.isDisplayed(), "Text button is not visible");

        // New paste content
        WebElement pasteContentField = driver.findElement(By.xpath
                ("//div[@class='de1']"));
        String expectedContent = "Hello from WebDriver";
        String actualContent = pasteContentField.getText();
        Assert.assertTrue(actualContent.contains(expectedContent),
                "Actual content does not contain the expected content\n " +
                        "Actual content: " + actualContent +
                        "\nExpected content: " + expectedContent);
    }


    // Helper method for handling the privacy banner
    public static void checkAndAcceptPrivacyBanner (WebDriver driver) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            WebElement agreeButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class=' css-47sehv']")));
            agreeButton.click();
        } catch (Exception e) { }
    }

    @AfterMethod(alwaysRun = true)
    private void tearDown() {
        driver.quit();
    }
}

